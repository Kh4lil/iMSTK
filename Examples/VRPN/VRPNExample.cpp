/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	  http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#include "imstkSimulationManager.h"
#include "imstkPbdModel.h"
#include "imstkPbdObject.h"
#include "imstkAPIUtilities.h"
#include "imstkSurfaceMesh.h"
#include "imstkCollisionGraph.h"
#include "imstkCamera.h"
#include "imstkLight.h"
#include "imstkScene.h"

//new
#include "imstkMeshIO.h"
#include "imstkCameraController.h"
#include "imstkCube.h"
#include "imstkSceneObjectController.h"

//VRPN
#include <imstkVRPNArduinoDeviceClient.h>
#include <imstkVRPNDeviceClient.h>
#include <imstkVRPNDeviceServer.h>

using namespace imstk;

///
/// \brief This example demonstrates the data exchange between VRPN and imstk
///
int
main()
{
	auto simManager = std::make_shared<SimulationManager>();
	auto scene = simManager->createNewScene("VRPNExample");


	//VRPN:
 
	const std::string deviceName = "sa0";
	/// \ IP address of the server.
	const std::string serverIP = "";
	/// \ the default VRPN port 3883
	const int serverPort = 38833;


	// Device Client
	auto client = std::make_shared<VRPNDeviceClient>(deviceName);

	//VRPN Server
	auto server = std::make_shared<VRPNDeviceServer>(serverIP, serverPort);

	//Device 1:
	server->addDeviceClient(client, deviceName, imstk::DeviceType::Analog, 0);

	//server->addDevice("Mouse0", imstk::DeviceType::Analog, 1);
	simManager->addModule(server);

	
	// Load Mesh
	auto mesh = MeshIO::read(iMSTK_DATA_ROOT "/asianDragon/asianDragon.obj");
	auto meshObject = std::make_shared<VisualObject>("meshObject");
	meshObject->setVisualGeometry(mesh);
	scene->addSceneObject(meshObject);

	// Update Camera position
	auto camera = scene->getCamera();
	camera->setPosition(Vec3d(0, 0, 20));


	auto camController = std::make_shared<CameraController>(camera, client);

	//camController->setTranslationScaling(100);
	//LOG(INFO) << camController->getTranslationOffset(); // should be the same than initial cam position
	camController->setInversionFlags(CameraController::InvertFlag::rotY |
		CameraController::InvertFlag::rotZ);

	scene->addCameraController(camController);


	// Light
	auto light = std::make_shared<DirectionalLight>("light");
	light->setFocalPoint(Vec3d(5, -8, -5));
	light->setIntensity(1);
	scene->addLight(light);

	// Run
	simManager->setActiveScene(scene);
	simManager->start(SimulationStatus::Paused);
	

	return 0;
}