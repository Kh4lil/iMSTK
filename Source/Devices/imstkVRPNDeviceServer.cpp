/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/
//#pragma comment( lib, "Devices" )
#include "imstkVRPNDeviceServer.h"

#include "vrpn_3DConnexion.h"
#include "vrpn_Tracker_NovintFalcon.h"
#include "vrpn_Tracker_OSVRHackerDevKit.h"
#include "vrpn_Streaming_Arduino.h"
#ifdef VRPN_USE_PHANTOM_SERVER
  #include "vrpn_Phantom.h"
#endif

#include "imstkLogger.h"
#include <map>
#include <string>
#include <stdexcept>
#include "imstkVRPNDeviceClient.h"

namespace imstk
{
	/*

void
VRPNDeviceServer::addDevice(const std::string& deviceName, DeviceType deviceType, int id)
{
    m_deviceInfoMap[deviceName] = std::make_pair(deviceType, id);

    if (deviceType == DeviceType::PhantomOmni)
    {
        LOG(WARNING) << "VRPNDeviceServer::addDevice warning: OpenHaptics support on VRPN "
                     << "currently unstable for the Phantom Omni (no force feedback implemented).\n"
                     << "Use HDAPIDeviceClient instead of VRPNDeviceServer/Client for ";
    }
}
*/

void
VRPNDeviceServer::addDeviceClient(std::shared_ptr<VRPNDeviceClient> client, const std::string& deviceName, DeviceType deviceType, int id)
	{
		//m_deviceInfoMap[deviceName] = std::make_pair(deviceType, id);
		m_deviceClients2.push_back(client);
		m_deviceInfoMap[deviceName] = std::make_pair(deviceType, id);

		if (deviceType == DeviceType::PhantomOmni)
		{
			LOG(WARNING) << "VRPNDeviceServer::addDevice warning: OpenHaptics support on VRPN "
				<< "currently unstable for the Phantom Omni (no force feedback implemented).\n"
				<< "Use HDAPIDeviceClient instead of VRPNDeviceServer/Client for ";
		}
	}

void
VRPNDeviceServer::addSerialDevice(const std::string& deviceName, DeviceType deviceType, const std::string& port, int baudRate, int id)
{
    SerialInfo serialSettings;
    serialSettings.baudRate     = baudRate;
    serialSettings.port         = port;
    m_deviceInfoMap[deviceName] = std::make_pair(deviceType, id);
    m_SerialInfoMap[deviceName] = serialSettings;
}

void
VRPNDeviceServer::initModule()
{
    std::string ip = m_machine + ":" + std::to_string(m_port);

    m_serverConnection = vrpn_create_server_connection(ip.c_str());


    m_deviceConnections = new vrpn_MainloopContainer();

    for (const auto& device : m_deviceInfoMap)
    {
        std::string name = device.first;
        DeviceType  type = device.second.first;
        auto        id   = device.second.second;

		std::string address = name + "@" + m_machine;
		const char* _address = address.c_str();

        switch (type)
        {
        case DeviceType::Tracker:
        {
			SerialInfo connectionSettings = m_SerialInfoMap[name];
            //m_deviceConnections->add(new vrpn_3DConnexion_SpaceExplorer(name.c_str(), m_serverConnection));
			//vrpn_Tracker_Remote* vrpnTracker = new vrpn_Tracker_Remote(_address);
			//m_deviceConnections->add(vrpnTracker);
			//vrpnTracker->register_change_handler(0, VRPNDeviceClient::trackerChangeHandler);
			
        } break;
        case DeviceType::Button:
        {
			vrpn_Button_Remote* vrpnButton = new vrpn_Button_Remote(_address);
			m_deviceConnections->add(vrpnButton);

			vrpnButton->register_change_handler(0, VRPNDeviceClient::buttonChangeHandler);

        } break;
        case DeviceType::PhantomOmni:
        {
#ifdef VRPN_USE_PHANTOM_SERVER
            char* deviceName = const_cast<char*>(name.c_str());
            //m_deviceConnections->add(new vrpn_Phantom(deviceName, m_serverConnection, 90.0f, deviceName));
#else
            LOG(WARNING) << "VRPNDeviceServer::initModule error: no support for Phantom Omni in VRPN. "
                         << "Install OpenHaptics SDK, the omni driver, and build VRPN with VRPN_USE_PHANTOM_SERVER.";
#endif
        } break;

        case DeviceType::Analog:
        {
            SerialInfo connectionSettings = m_SerialInfoMap[name];
			vrpn_Analog_Remote* vrpnAnalog = new vrpn_Analog_Remote(_address);
			m_deviceConnections->add(vrpnAnalog);

			vrpnAnalog->register_change_handler(0, VRPNDeviceClient::analogChangeHandler);

        } break;
        default:
        {
            LOG(WARNING) << "VRPNDeviceServer::initModule error: can not connect to "
                         << name << ", device type unknown.";
        } break;	
        }
    }
}

void
VRPNDeviceServer::runModule()
{
    m_serverConnection->mainloop();
    m_deviceConnections->mainloop();
}

void
VRPNDeviceServer::cleanUpModule()
{
    m_deviceConnections->clear();
    delete(m_deviceConnections);

    m_serverConnection->removeReference();
    //delete(m_connection);
}
} // imstk
